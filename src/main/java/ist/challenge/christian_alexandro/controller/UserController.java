package ist.challenge.christian_alexandro.controller;

import ist.challenge.christian_alexandro.entity.UserEntity;
import ist.challenge.christian_alexandro.request.EditUserRequest;
import ist.challenge.christian_alexandro.service.EditUserService;
import ist.challenge.christian_alexandro.service.LoginService;
import ist.challenge.christian_alexandro.service.RegistrasiService;
import ist.challenge.christian_alexandro.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static ist.challenge.christian_alexandro.util.UserUtil.checkUsernameAndPasswordNotNull;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {
    private final LoginService loginService;
    private final RegistrasiService registrasiService;
    private final UserService userService;
    private final EditUserService editUserService;

    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody UserEntity user) {
        return loginService.login(user);
    }

    @PostMapping("/registrasi")
    public ResponseEntity<String> registrasi(@RequestBody UserEntity user) {
        return registrasiService.registrasi(user);
    }

    @GetMapping("/")
    public ResponseEntity<List<UserEntity>> users() {
        return userService.finAlldUser();
    }

    @GetMapping("/search/")
    public ResponseEntity<UserEntity> users(@RequestParam("username") String username) {
        return userService.findUser(username);
    }

    @PutMapping("/edit")
    public ResponseEntity<String> edit(@RequestBody EditUserRequest request) {
        return editUserService.edit(request);
    }
}
