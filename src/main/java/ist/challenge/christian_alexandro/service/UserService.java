package ist.challenge.christian_alexandro.service;

import ist.challenge.christian_alexandro.entity.UserEntity;
import ist.challenge.christian_alexandro.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;

    public ResponseEntity<UserEntity> findUser(String username) {
        UserEntity user = userRepository.findByUsername(username).orElse(null);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    public ResponseEntity<List<UserEntity>> finAlldUser() {
        List<UserEntity> allUsers = userRepository.findAll();
        return new ResponseEntity<>(allUsers, HttpStatus.OK);
    }
}
