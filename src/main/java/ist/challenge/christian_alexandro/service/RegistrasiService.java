package ist.challenge.christian_alexandro.service;

import ist.challenge.christian_alexandro.entity.UserEntity;
import ist.challenge.christian_alexandro.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import static ist.challenge.christian_alexandro.util.UserUtil.checkUsernameAndPasswordNotNull;

@Service
@RequiredArgsConstructor
public class RegistrasiService {
    private final UserRepository userRepository;

    public ResponseEntity<String> registrasi(UserEntity user) {
        String isEmpty = checkUsernameAndPasswordNotNull(user);

        if (isEmpty != null) {
            return new ResponseEntity<>(isEmpty, HttpStatus.BAD_REQUEST);
        }

        boolean isUserExists = userRepository.findByUsername(user.getUsername()).isPresent();

        if (isUserExists){
            return new ResponseEntity<>("Username sudah terpakai", HttpStatus.CONFLICT) ;
        }

        UserEntity reponse = userRepository.save(user);

        if (reponse != null){
            return new ResponseEntity<>("Registrasi berhasil", HttpStatus.CREATED) ;
        }

        return new ResponseEntity<>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
