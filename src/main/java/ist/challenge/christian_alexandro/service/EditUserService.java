package ist.challenge.christian_alexandro.service;

import ist.challenge.christian_alexandro.entity.UserEntity;
import ist.challenge.christian_alexandro.repository.UserRepository;
import ist.challenge.christian_alexandro.request.EditUserRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EditUserService {
    private final UserRepository userRepository;

    public ResponseEntity<String> edit(EditUserRequest user){
        boolean checkUser = userRepository.findByUsername(user.getNewUsername()).isPresent();
        if(checkUser){
            return new ResponseEntity<>("Username sudah terpakai", HttpStatus.CONFLICT);
        }

        if(user.getNewPassword().equals(user.getOldPassword())){
            return new ResponseEntity<>("Password tidak boleh sama dengan password sebelumnya",
                    HttpStatus.BAD_REQUEST);
        }

        UserEntity userDatum = userRepository.findByUsername(user.getOldUsername()).orElse(null);
        userDatum.setUsername(user.getNewUsername());
        userDatum.setPassword(user.getNewPassword());
        userRepository.save(userDatum);

        return new ResponseEntity<>(HttpStatus.CREATED);

    }
}
