package ist.challenge.christian_alexandro.service;

import ist.challenge.christian_alexandro.entity.UserEntity;
import ist.challenge.christian_alexandro.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import static ist.challenge.christian_alexandro.util.UserUtil.checkUsernameAndPasswordNotNull;

@Service
@RequiredArgsConstructor
public class LoginService {
    private final UserRepository userRepository;

    public ResponseEntity<String> login(UserEntity user) {
        String isEmpty = checkUsernameAndPasswordNotNull(user);

        if (isEmpty != null) {
            return new ResponseEntity<>(isEmpty, HttpStatus.BAD_REQUEST);
        }
        
        UserEntity userResponse = userRepository.findByUsernameAndPassword(user.getUsername(), user.getPassword());
        if (userResponse != null) {
            return new ResponseEntity<>("Sukses Login", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Gagal Login", HttpStatus.UNAUTHORIZED);
        }
    }
}
