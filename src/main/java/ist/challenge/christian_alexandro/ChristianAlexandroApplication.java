package ist.challenge.christian_alexandro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChristianAlexandroApplication {


	public static void main(String[] args) {
		SpringApplication.run(ChristianAlexandroApplication.class, args);
	}

}
