package ist.challenge.christian_alexandro.repository;


import ist.challenge.christian_alexandro.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {
    UserEntity findByUsernameAndPassword(String username, String password);

    Optional<UserEntity> findByUsername(String username);
}
